function initApp() {
    // Login with Email/Password
    var txtEmail = document.getElementById('inputEmail');
    var txtPassword = document.getElementById('inputPassword');
    var txtName = document.getElementById('inputName');
    var btnLogin = document.getElementById('btnLogin');
    var btnGoogle = document.getElementById('btngoogle');
    var btnSignUp = document.getElementById('btnSignUp');
    btnLogin.addEventListener('click', function() {
        firebase.auth().signInWithEmailAndPassword(txtEmail.value, txtPassword.value).then(function(result) { 
            window.location.replace("../index.html");
        }).catch(function(error) { 
            var errorCode = error.code; 
            var errorMessage = error.message; 
            var email = error.email; 
            var credential = error.credential; 
            alert(errorMessage);
        });
    });

    btnGoogle.addEventListener('click', function() {
        var provider = new firebase.auth.GoogleAuthProvider();
        firebase.auth().signInWithPopup(provider).then(function(result) { 
            firebase.database().ref(result.user.displayName + '/' + result.user.displayName).push({
                name: result.user.displayName,
                message: ''
            });
            setTimeout(() => {window.location.replace("../index.html");}, 1000);
        }).catch(function(error) { 
            var errorCode = error.code; 
            var errorMessage = error.message; 
            var email = error.email; 
            var credential = error.credential; 
            alert(errorMessage);
        });
    });

    btnSignUp.addEventListener('click', function() {
        if(txtName.value == ''){
            alert('Name can\'t be empty');
        }
        else{
            firebase.auth().createUserWithEmailAndPassword(txtEmail.value,txtPassword.value).then(function(result) { 
                result.user.updateProfile({
                    displayName: txtName.value
                });
                firebase.database().ref(txtName.value + '/' + txtName.value).push({
                    name: txtName.value,
                    message: ''
                });
                setTimeout(() => {window.location.replace("../index.html");}, 1000);
            }).catch(function(error) {
                txtEmail.value = "";
                txtPassword.value = "";
                var errorCode = error.code; 
                var errorMessage = error.message; 
                var email = error.email; 
                var credential = error.credential; 
                alert(errorMessage);
            });
        }
    });
}

// Custom alert
function create_alert(type, message) {
    var alertarea = document.getElementById('custom-alert');
    if (type == "success") {
        str_html = "<div class='alert alert-success alert-dismissible fade show' role='alert'><strong>Success! </strong>" + message + "<button type='button' class='close' data-dismiss='alert' aria-label='Close'><span aria-hidden='true'>&times;</span></button></div>";
        alertarea.innerHTML = str_html;
    } else if (type == "error") {
        str_html = "<div class='alert alert-danger alert-dismissible fade show' role='alert'><strong>Error! </strong>" + message + "<button type='button' class='close' data-dismiss='alert' aria-label='Close'><span aria-hidden='true'>&times;</span></button></div>";
        alertarea.innerHTML = str_html;
    }
}

window.onload = function() {
    initApp();
};