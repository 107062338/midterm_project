var user_email = '';
var user_name = '';
var current_contact;
var current_path;
var friend_path;
var first_count = 0;
var second_count = 0;
function init() {
    Notification.requestPermission(function (status) {
        if (Notification.permission !== status) {
            Notification.permission = status;
        }
    });
    firebase.auth().onAuthStateChanged(function (user) {
        // Check user login
        if (user) {
            current_path = '/lobby';
            current_contact = 'lobby';
            user_email = user.email;
            user_name = user.displayName;
            document.getElementById('profile').style.display = '';
            document.getElementById('logout').style.display = '';
            document.getElementById('search').style.display = '';
            document.getElementById('login').style.display = 'none';
            document.getElementById('username').innerHTML = user_name;
            document.getElementById('username1').innerHTML = user_name;
            document.getElementById('facebook').value = user_name;
            document.getElementById('twitter').value = user_name;
            document.getElementById('instagram').value = user_name;
            document.getElementById('logout').addEventListener('click', function () {
                firebase.auth().signOut().then(function (result) {
                    alert('Signed Out');
                }).catch(function (error) {
                    alert(error.message);
                })
            });
            room_build('lobby').then(function (result) {
                document.getElementById('list-lobby').className = 'contact glow-on-hover-active';
                document.getElementById('profile-lobby').style.display = '';
                document.getElementById('messages-lobby').style.display = '';
                var first = 0, second = 0;
                firebase.database().ref('/lobby').once('value').then(function (snapshot) {
                    snapshot.forEach(function (childSnapshot) {
                        first++;
                        var childData = childSnapshot.val();
                        if (childData.name == user_name) {
                            document.getElementById('ul-lobby').innerHTML += '<li class="replies"><sub style="float: right;">' + childData.name + '</sub><p>' + childData.message + '</p></li>';
                        }
                        else {
                            document.getElementById('ul-lobby').innerHTML += '<li class="sent"><sub style="float: left;">' + childData.name + '</sub><p>' + childData.message + '</p></li>';
                        }
                    });
                    firebase.database().ref('/lobby').on('child_added', function (data) {
                        second++;
                        if (second > first) {
                            var childData = data.val();
                            if (childData.name == user_name) {
                                document.getElementById('ul-lobby').innerHTML += '<li class="replies"><sub style="float: right;">' + childData.name + '</sub><p>' + childData.message + '</p></li>';
                            }
                            else {
                                var n = new Notification(childData.name + ' sent a message:' + childData.message);
                                document.getElementById('ul-lobby').innerHTML += '<li class="sent"><sub style="float: left;">' + childData.name + '</sub><p>' + childData.message + '</p></li>';
                            }
                        }
                    });
                }).catch(function (error) {
                    alert(error.message);
                });
                document.getElementById('list-lobby').addEventListener('click', function () {
                    if (current_contact != 'lobby') {
                        current_path = '/lobby';
                        document.getElementById('list-' + current_contact).className = 'contact glow-on-hover';
                        document.getElementById('list-lobby').className = 'contact glow-on-hover-active';
                        document.getElementById('profile-' + current_contact).style.display = 'none';
                        document.getElementById('messages-' + current_contact).style.display = 'none';
                        document.getElementById('profile-lobby').style.display = '';
                        document.getElementById('messages-lobby').style.display = '';
                        current_contact = 'lobby';
                    }
                });
            });
            firebase.database().ref('/' + user_name).on('child_added', function (childSnapshot) {
                if (childSnapshot.key != user_name) {
                    room_build(childSnapshot.key).then(function (result) {
                        var mes = document.getElementById('ul-' + childSnapshot.key);
                        var first = 0, second = 0;
                        firebase.database().ref('/' + user_name + '/' + childSnapshot.key).once('value').then(function (snapshot) {
                            snapshot.forEach(function (childSnapshot) {
                                first++;
                                var childData = childSnapshot.val();
                                if (childData.message != '') {
                                    if (childData.name == user_name) {
                                        mes.innerHTML += '<li class="replies"><sub style="float: right;">' + childData.name + '</sub><p>' + childData.message + '</p></li>';
                                    }
                                    else {
                                        mes.innerHTML += '<li class="sent"><sub style="float: left;">' + childData.name + '</sub><p>' + childData.message + '</p></li>';
                                    }
                                }
                            });
                            firebase.database().ref('/' + user_name + '/' + childSnapshot.key).on('child_added', function (data) {
                                second++;
                                if (second > first) {
                                    var childData = data.val();
                                    if (childData.message != '') {
                                        if (childData.name == user_name) {
                                            mes.innerHTML += '<li class="replies"><sub style="float: right;">' + childData.name + '</sub><p>' + childData.message + '</p></li>';
                                        }
                                        else {
                                            var n = new Notification(childData.name + ' sent a message:' + childData.message);
                                            mes.innerHTML += '<li class="sent"><sub style="float: left;">' + childData.name + '</sub><p>' + childData.message + '</p></li>';
                                        }
                                    }
                                }
                            });
                        }).catch(function (error) {
                            alert(error.message);
                        });
                        document.getElementById('list-' + childSnapshot.key).addEventListener('click', function () {
                            friend_path = '/' + childSnapshot.key + '/' + user_name;
                            current_path = '/' + user_name + '/' + childSnapshot.key;
                            document.getElementById('list-' + current_contact).className = 'contact glow-on-hover';
                            document.getElementById('list-' + childSnapshot.key).className = 'contact glow-on-hover-active';
                            document.getElementById('profile-' + current_contact).style.display = 'none';
                            document.getElementById('messages-' + current_contact).style.display = 'none';
                            document.getElementById('profile-' + childSnapshot.key).style.display = '';
                            document.getElementById('messages-' + childSnapshot.key).style.display = '';
                            current_contact = childSnapshot.key;
                        });
                    });
                }
            });
        } else {
            document.getElementById('search').style.display = 'none';
            document.getElementById('login').style.display = '';
            document.getElementById('profile').style.display = 'none';
            document.getElementById('username1').style.display = 'none';
            document.getElementById('logout').style.display = 'none';
            document.getElementById('contacts-list').innerHTML = '';
            document.getElementById('content-list').innerHTML = '<div class="message-input"><div class="wrap"><input id="sent-txt" type="text" placeholder="Write your message..." /><i class="fa fa-paperclip attachment" aria-hidden="true"></i><button id="sent-btn" class="submit"><i class="fa fa-paper-plane" aria-hidden="true"></i></button></div></div>';
        }
    });

    sent_txt = document.getElementById('sent-txt');
    document.getElementById('sent-btn').addEventListener('click', function () {
        if (sent_txt.value != "") {
            sent_txt.value = sent_txt.value.replace('<', '&lt');
            for (var i = 0; i < sent_txt.value.length; i++) {
                if (sent_txt.value[i] == '<') {
                    sent_txt.value = sent_txt.value.substring(0, i) + '&lt;' + sent_txt.value.substring(i + 1, sent_txt.value.length);
                }
            }
            if (current_path == '/lobby') {
                firebase.database().ref(current_path).push({
                    name: user_name,
                    message: sent_txt.value.replace('<', '&lt;')
                });
            }
            else {
                firebase.database().ref(current_path).push({
                    name: user_name,
                    message: sent_txt.value
                });
                firebase.database().ref(friend_path).push({
                    name: user_name,
                    message: sent_txt.value
                });
            }
            sent_txt.value = "";
        }
    });
}

window.onload = function () {
    init();
    $("#addcontact").click(function () {
        if(document.getElementById('friend-name').value == ''){
            alert('Name can\'t be empty');
        }
        else{
            firebase.database().ref('/').once('value').then(function (snapshot) {
                if (snapshot.hasChild(document.getElementById('friend-name').value)) {
                    create_chatroom().then(function (result) {
                        document.getElementById('friend-name').value = '';
                    }).catch(function (error) {
                        alert(error.message);
                    });
                }
                else {
                    alert("Friend's name not found");
                }
            }).catch(function (error) {
                alert(error.message);
            });
        }
    });
    $(window).on('keydown', function (e) {
        if (e.which == 13) {
            if (sent_txt.value != "") {
                sent_txt.value = sent_txt.value.replace('<', '&lt');
                for (var i = 0; i < sent_txt.value.length; i++) {
                    if (sent_txt.value[i] == '<') {
                        sent_txt.value = sent_txt.value.substring(0, i) + '&lt;' + sent_txt.value.substring(i + 1, sent_txt.value.length);
                    }
                }
                if (current_path == '/lobby') {
                    firebase.database().ref(current_path).push({
                        name: user_name,
                        message: sent_txt.value
                    });
                }
                else {
                    firebase.database().ref(current_path).push({
                        name: user_name,
                        message: sent_txt.value
                    });
                    firebase.database().ref(friend_path).push({
                        name: user_name,
                        message: sent_txt.value
                    });
                }
                sent_txt.value = "";
            }
        }
    });
};


function create_chatroom() {
    return new Promise(function (resolve, reject) {
        firebase.database().ref('/' + document.getElementById('friend-name').value).once('value').then(function (snapshot) {
            if (snapshot.hasChild(user_name)) {
                reject('Chat-Exist');
            }
            else {
                firebase.database().ref(user_name + '/' + document.getElementById('friend-name').value).push({
                    name: user_name,
                    message: ''
                });
                firebase.database().ref(document.getElementById('friend-name').value + '/' + user_name).push({
                    name: user_name,
                    message: ''
                });
                setTimeout(() => { resolve("creat success"); }, 100);
            }
        }).catch(function (error) {
            reject(error);
        });
    });
}
function room_build(room_name) {
    return new Promise(function (resolve, reject) {
        var list = document.createElement("li");
        list.id = 'list-' + room_name;
        list.className = 'contact glow-on-hover';
        var li_div = document.createElement("div");
        li_div.className = 'wrap';
        var li_span = document.createElement('span');
        li_span.className = 'contact-status online';
        var li_in_div = document.createElement("div");
        li_in_div.className = 'meta';
        var li_name = document.createElement("p");
        li_name.className = 'name';
        li_name.innerHTML = room_name;
        var li_pre = document.createElement("p");
        li_pre.className = 'preview';
        list.appendChild(li_div);
        li_div.appendChild(li_span);
        li_div.appendChild(li_in_div);
        li_in_div.appendChild(li_name);
        li_in_div.appendChild(li_pre);
        document.getElementById('contacts-list').appendChild(list);

        var con_div, con_p, con_in_div, con_i1, con_i2, con_i3;
        con_div = document.createElement('div');
        con_div.className = 'contact-profile';
        con_div.id = 'profile-' + room_name;
        con_div.style.display = 'none';
        con_p = document.createElement('p');
        con_p.innerHTML = room_name;
        con_in_div = document.createElement('div');
        con_in_div.className = 'social-media';
        con_i1 = document.createElement('i');
        con_i1.className = 'fa fa-facebook';
        con_i1.setAttribute('aria-hidden', 'true');
        con_i2 = document.createElement('i');
        con_i2.className = 'fa fa-twitter';
        con_i2.setAttribute('aria-hidden', 'true');
        con_i3 = document.createElement('i');
        con_i3.className = 'fa fa-instagram';
        con_i3.setAttribute('aria-hidden', 'true');
        con_div.appendChild(con_p);
        con_div.appendChild(con_in_div);
        con_in_div.appendChild(con_i1);
        con_in_div.appendChild(con_i2);
        con_in_div.appendChild(con_i3);
        document.getElementById('content-list').appendChild(con_div);
        var mes_div, mes_ul;
        mes_div = document.createElement('div');
        mes_div.id = 'messages-' + room_name;
        mes_div.className = 'messages';
        mes_div.style.display = 'none';
        mes_ul = document.createElement('ul');
        mes_ul.id = 'ul-' + room_name;
        mes_div.appendChild(mes_ul);
        document.getElementById('content-list').appendChild(mes_div);
        //
        setTimeout(() => { resolve('room_tag_build'); }, 100);
    });
}