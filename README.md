# Midterm_Project

# 作品網址：https://midproject107062338.web.app

## Basic Components
|Component|Score|Y/N|
|:-:|:-:|:-:|
|Membership Mechanism|15%|Y|
|Firebase Page|5%|Y|
|Database|15%|Y|
|RWD|15%|Y|
|Topic Key Function|20%|Y|

## Advanced Components
|Component|Score|Y/N|
|:-:|:-:|:-:|
|Third-Party Sign In|2.5%|Y|
|Chrome Notification|5%|Y|
|Use CSS Animation|2.5%|Y|
|Security Report|5%|N|

**READEME.md**
* [x]  Report

## v1
*  Implement chat room functions with lab-06 template

## v2
*  Change chatroom/login page appearence to other template

## v3
* DeBug

## v4
* Add animation component

### Key functions(How to use)

---

#### Create an account
1.  點下login，進入login頁面。
2.  填入Email, Password, Name欄位，按下SignUP，註冊成功將自動導回chat room。

#### Login
1.  填入註冊的Email, Password(不須Name)，按下Login，登入成功自動導回chatroom。
2.  使用Google登入，按下Login via Google，選擇帳號後，登入成功自動導回chatroom

#### Logout
1.  登入後在chatroom頁面會有logout，按下後即可登出

#### Create a chat room
1.  在左下方的input裡填入已註冊的用戶Name，再按下，加入成功會在聯絡人欄位新增好友名字。
2.  點選好友即可切換至該聊天室，並且開始聊天。
